## Interface: 70300
## Title: Lib: LibArtifactPower-1.0
## Notes: LibArtifactPower lets you calculate the amount of AP an item will give you without needing to scan the tooltip
## Author: Infinitron
## X-Category: Library
## X-License: All Rights Reserved
## Version: 1.0

LibStub\LibStub.lua
LibArtifactPower-1.0.lua
