## Interface: 70300
## LoadOnDemand: 1

## Title: Lib: DataBroker-1.0
## Notes: Provides a MVC interface for addons to detach from the display addon.
## Author: Tekkub
## Version: 3.0-100
## X-Website: https://github.com/tekkub/libdatabroker-1-1/
## X-Category: Library

## X-Revision: 100
## X-Date: 2018-03-19T20:23:06Z

LibDataBroker-1.1.lua
