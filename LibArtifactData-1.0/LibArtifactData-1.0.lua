local MAJOR, MINOR = "LibArtifactData-1.0", 21

assert(_G.LibStub, MAJOR .. " requires LibStub")
local lib = _G.LibStub:NewLibrary(MAJOR, MINOR)
if not lib then return end

lib.callbacks = lib.callbacks or _G.LibStub("CallbackHandler-1.0"):New(lib)

local Debug = function() end
if _G.AdiDebug then
	Debug = _G.AdiDebug:Embed({}, MAJOR)
end

local artifactPowerData = {
	multiplier = {
		[1] = 1,
		[2] = 1.25,
		[3] = 1.5,
		[4] = 1.9,
		[5] = 2.4,
		[6] = 3,
		[7] = 3.75,
		[8] = 4.75,
		[9] = 6,
		[10] = 7.5,
		[11] = 9.5,
		[12] = 12,
		[13] = 15,
		[14] = 18.75,
		[15] = 23.5,
		[16] = 29.5,
		[17] = 37,
		[18] = 46.5,
		[19] = 58,
		[20] = 73,
		[21] = 91,
		[22] = 114,
		[23] = 143,
		[24] = 179,
		[25] = 224,
		[26] = 250,
		[27] = 1001,
		[28] = 1301,
		[29] = 1701,
		[30] = 2201,
		[31] = 2901,
		[32] = 3801,
		[33] = 4901,
		[34] = 6401,
		[35] = 8301,
		[36] = 10801,
		[37] = 14001,
		[38] = 18201,
		[39] = 23701,
		[40] = 30801,
		[41] = 40001,
		[42] = 160001,
		[43] = 208001,
		[44] = 270401,
		[45] = 351501,
		[46] = 457001,
		[47] = 594001,
		[48] = 772501,
		[49] = 1004001,
		[50] = 1305001,
		[51] = 1696501,
		[52] = 2205501,
		[53] = 2867501,
		[54] = 3727501,
		[55] = 4846001,
		[56] = 6300001,
		[57] = 6300001,
		[58] = 6300001,
		[59] = 6300001,
		[60] = 6300001,
	},
	spells = {
        [179492] = 300,
        [179958] = 300,
        [179959] = 200,
        [179960] = 200,
        [181851] = 300,
        [181852] = 200,
        [181854] = 300,
        [187511] = 1000,
        [187536] = 300,
        [188542] = 7,
        [188543] = 19,
        [188627] = 19,
        [188642] = 7,
        [188656] = 79,
        [190599] = 100,
        [192731] = 150,
        [196374] = 50,
        [196461] = 5,
        [196493] = 50,
        [196499] = 75,
        [196500] = 100,
        [193823] = 250,
        [199685] = 1000,
        [201742] = 100,
        [204695] = 1000,
        [205057] = 1,
        [207600] = 300,
        [216876] = 10,
        [217024] = 400,
        [217026] = 25,
        [217045] = 75,
        [217055] = 100,
        [217299] = 35,
        [217300] = 35,
        [217301] = 100,
        [217355] = 100,
        [217511] = 50,
        [217512] = 60,
        [217670] = 200,
        [217671] = 400,
        [217689] = 150,
        [220547] = 100,
        [220548] = 235,
        [220549] = 480,
        [220550] = 450,
        [220551] = 530,
        [220553] = 550,
        [220784] = 200,
        [224139] = 25,
        [224544] = 25,
        [224583] = 25,
        [224585] = 25,
        [224593] = 25,
        [224595] = 25,
        [224608] = 25,
        [224610] = 25,
        [224633] = 25,
        [224635] = 25,
        [224641] = 25,
        [224643] = 25,
        [225897] = 100,
        [227531] = 200,
        [227535] = 300,
        [227886] = 545,
        [227889] = 210,
        [227904] = 35,
        [227905] = 55,
        [227907] = 200,
        [227941] = 150,
        [227942] = 200,
        [227943] = 465,
        [227944] = 520,
        [227945] = 165,
        [227946] = 190,
        [227947] = 210,
        [227948] = 230,
        [227949] = 475,
        [227950] = 515,
        [228067] = 400,
        [228069] = 100,
        [228078] = 500,
        [228079] = 600,
        [228080] = 250,
        [228106] = 490,
        [228107] = 250,
        [228108] = 210,
        [228109] = 170,
        [228110] = 205,
        [228111] = 245,
        [228112] = 160,
        [228130] = 125,
        [228131] = 400,
        [228135] = 250,
        [228220] = 150,
        [228310] = 50,
        [228352] = 500,
        [228422] = 175,
        [228423] = 350,
        [228436] = 170,
        [228437] = 220,
        [228438] = 195,
        [228439] = 185,
        [228440] = 190,
        [228442] = 215,
        [228443] = 180,
        [228444] = 750,
        [228647] = 400,
        [228921] = 500,
        [228955] = 25,
        [228956] = 50,
        [228957] = 35,
        [228959] = 45,
        [228960] = 20,
        [228961] = 25,
        [228962] = 40,
        [228963] = 80,
        [228964] = 150,
        [229746] = 100,
        [229747] = 200,
        [229776] = 1000,
        [229778] = 100,
        [229779] = 300,
        [229780] = 350,
        [229781] = 300,
        [229782] = 500,
        [229783] = 100,
        [229784] = 150,
        [229785] = 800,
        [229786] = 350,
        [229787] = 300,
        [229788] = 600,
        [229789] = 250,
        [229790] = 2000,
        [229791] = 1000,
        [229792] = 4000,
        [229793] = 900,
        [229794] = 1000,
        [229795] = 650,
        [229796] = 450,
        [229798] = 750,
        [229799] = 1200,
        [229803] = 500,
        [229804] = 875,
        [229805] = 1250,
        [229806] = 2500,
        [229807] = 20,
        [229857] = 100,
        [229858] = 100,
        [229859] = 1000,
        [231035] = 100,
        [231041] = 100,
        [231047] = 1000,
        [231048] = 500,
        [231337] = 600,
        [231362] = 200,
        [231453] = 500,
        [231512] = 500,
        [231538] = 250,
        [231543] = 500,
        [231544] = 100,
        [231556] = 500,
        [231581] = 250,
        [231647] = 500,
        [231669] = 500,
        [231709] = 500,
        [231727] = 800,
        [232755] = 90,
        [232832] = 95,
        [232890] = 400,
        [232994] = 100,
        [232995] = 120,
        [232996] = 180,
        [232997] = 800,
        [233030] = 150,
        [233031] = 100,
        [233204] = 500,
        [233209] = 500,
        [233211] = 800,
        [233242] = 300,
        [233243] = 1000,
        [233244] = 250,
        [233245] = 250,
        [233348] = 3000,
        [233816] = 250,
        [234045] = 250,
        [234047] = 400,
        [234048] = 500,
        [234049] = 600,
        [235245] = 175,
        [235246] = 195,
        [235247] = 220,
        [235248] = 240,
        [235256] = 250,
        [235257] = 155,
        [235266] = 500,
        [237344] = 320,
        [237345] = 380,
        [238029] = 85,
        [238030] = 115,
        [238031] = 300,
        [238032] = 400,
        [238033] = 750,
        [238252] = 85,
        [239094] = 600,
        [239095] = 650,
        [239096] = 270,
        [239097] = 225,
        [239098] = 285,
        [240331] = 200,
        [240332] = 125,
        [240333] = 600,
        [240335] = 240,
        [240337] = 360,
        [240339] = 1600,
        [240483] = 2500,
        [241156] = 175,
        [241157] = 290,
        [241158] = 325,
        [241159] = 465,
        [241160] = 300,
        [241161] = 475,
        [241162] = 540,
        [241163] = 775,
        [241164] = 375,
        [241165] = 600,
        [241166] = 675,
        [241167] = 1000,
        [241471] = 750,
        [241476] = 1000,
        [241752] = 800,
        [241753] = 255,
        [242062] = 500,
        [242116] = 3125,
        [242117] = 2150,
        [242118] = 1925,
        [242119] = 1250,
        [242564] = 1200,
        [242572] = 725,
        [242573] = 1500,
        [242575] = 5000,
        [242884] = 625,
        [242886] = 125,
        [242887] = 100,
        [242890] = 50,
        [242891] = 500,
        [242893] = 250,
        [242911] = 2000,
        [242912] = 400,
        [244814] = 600,
        [246165] = 500,
        [246166] = 525,
        [246167] = 625,
        [246168] = 275,
        [247040] = 750,
        [247075] = 250,
        [247316] = 450,
        [247319] = 125,
        [247631] = 300,
        [247633] = 700,
        [247634] = 1000,
        [248047] = 800,
        [248841] = 20,
        [248842] = 30,
        [248843] = 40,
        [248844] = 50,
        [248845] = 60,
        [248846] = 70,
        [248847] = 80,
        [248848] = 90,
        [248849] = 100,
        [250374] = 550,
        [250375] = 590,
        [250376] = 575,
        [250377] = 625,
        [250378] = 610,
        [250379] = 650,
        [251039] = 3500,
        [252078] = 200,
        [253833] = 400,
        [253834] = 600,
        [253902] = 1200,
        [253931] = 875,
        [254000] = 10000,
        [254387] = 500,
        [254593] = 200,
        [254603] = 570,
        [254608] = 630,
        [254609] = 565,
        [254610] = 635,
        [254656] = 645,
        [254657] = 745,
        [254658] = 550,
        [254659] = 650,
        [254660] = 640,
        [254661] = 560,
        [254662] = 625,
        [254663] = 575,
        [254699] = 50,
        [254761] = 750,
        [255161] = 650,
        [255162] = 550,
        [255163] = 750,
        [255165] = 800,
        [255166] = 600,
        [255167] = 900,
        [255168] = 1000,
        [255169] = 1250,
        [255170] = 1000,
        [255171] = 450,
        [255172] = 600,
        [255173] = 750,
        [255175] = 850,
        [255176] = 600,
        [255177] = 520,
        [255178] = 550,
        [255179] = 535,
        [255180] = 305,
        [255181] = 315,
        [255182] = 330,
        [255183] = 345,
        [255184] = 350,
        [255185] = 555,
        [255186] = 60,
        [255187] = 90,
        [255188] = 75
	},
	items = {
		[127999] = 10, -- Shard of Potentiation
		[128000] = 25, -- Crystal of Ensoulment
		[128021] = 25, -- Scroll of Enlightenment
		[128022] = 10, -- Treasured Coin
		[128026] = 150, -- Trembling Phylactery
		[130144] = 50, -- Crystallized Fey Darter Egg
		[130149] = 100, -- Carved Smolderhide Figurines
		[130153] = 100, -- Godafoss Essence
		[130159] = 100, -- Ravencrest Shield
		[130160] = 100, -- Vial of Pure Moonrest Water
		[130165] = 75, -- Heathrow Keepsake
		[131728] = 75, -- Urn of Malgalor's Blood
		[131758] = 50, -- Oversized Acorn
		[131778] = 50, -- Woodcarved Rabbit
		[131784] = 50, -- Left Half of a Locket
		[131785] = 50, -- Right Half of a Locket
		[131789] = 75, -- Handmade Mobile
		[132361] = 50, -- Petrified Arkhana
		[132923] = 50, -- Hrydshal Etching
		[134118] = 150, -- Cluster of Potentiation
		[134133] = 150, -- Jewel of Brilliance
		[138726] = 10, -- Shard of Potentiation
        [138839] = 50  -- Valiant's Glory
	}
}

-- local store
local artifacts = {}
local equippedID, viewedID, activeID
artifacts.knowledgeLevel = 0
artifacts.knowledgeMultiplier = 1

-- constants
local _G                       = _G
local BACKPACK_CONTAINER       = _G.BACKPACK_CONTAINER
local BANK_CONTAINER           = _G.BANK_CONTAINER
local INVSLOT_MAINHAND         = _G.INVSLOT_MAINHAND
local LE_ITEM_CLASS_ARMOR      = _G.LE_ITEM_CLASS_ARMOR
local LE_ITEM_CLASS_WEAPON     = _G.LE_ITEM_CLASS_WEAPON
local LE_ITEM_QUALITY_ARTIFACT = _G.LE_ITEM_QUALITY_ARTIFACT
local NUM_BAG_SLOTS            = _G.NUM_BAG_SLOTS
local NUM_BANKBAGSLOTS         = _G.NUM_BANKBAGSLOTS

-- blizzard api
local aUI                              = _G.C_ArtifactUI
local Clear                            = aUI.Clear
local GetArtifactInfo                  = aUI.GetArtifactInfo
local GetArtifactKnowledgeLevel        = aUI.GetArtifactKnowledgeLevel or _G.nop
local GetArtifactKnowledgeMultiplier   = aUI.GetArtifactKnowledgeMultiplier or _G.nop
local GetContainerItemInfo             = _G.GetContainerItemInfo
local GetContainerNumSlots             = _G.GetContainerNumSlots
local GetCostForPointAtRank            = aUI.GetCostForPointAtRank
local GetEquippedArtifactInfo          = aUI.GetEquippedArtifactInfo
local GetInventoryItemEquippedUnusable = _G.GetInventoryItemEquippedUnusable
local GetItemInfo                      = _G.GetItemInfo
local GetItemSpell                     = _G.GetItemSpell
local GetNumObtainedArtifacts          = aUI.GetNumObtainedArtifacts
local GetNumPurchasableTraits          = _G.ArtifactBarGetNumArtifactTraitsPurchasableFromXP or _G.MainMenuBar_GetNumArtifactTraitsPurchasableFromXP
local GetNumRelicSlots                 = aUI.GetNumRelicSlots
local GetPowerInfo                     = aUI.GetPowerInfo
local GetPowers                        = aUI.GetPowers
local GetRelicInfo                     = aUI.GetRelicInfo
local GetRelicLockedReason             = aUI.GetRelicLockedReason
local GetRelicSlotRankInfo             = aUI.GetRelicSlotRankInfo or _G.nop
local GetSpellInfo                     = _G.GetSpellInfo
local HasArtifactEquipped              = _G.HasArtifactEquipped
local IsArtifactPowerItem              = _G.IsArtifactPowerItem
local IsAtForge                        = aUI.IsAtForge
local IsViewedArtifactEquipped         = aUI.IsViewedArtifactEquipped
local SocketContainerItem              = _G.SocketContainerItem
local SocketInventoryItem              = _G.SocketInventoryItem

-- lua api
local select   = _G.select
local strmatch = _G.string.match
local tonumber = _G.tonumber

local private = {} -- private space for the event handlers

lib.frame = lib.frame or _G.CreateFrame("Frame")
local frame = lib.frame
frame:UnregisterAllEvents() -- deactivate old versions
frame:SetScript("OnEvent", function(_, event, ...) private[event](event, ...) end)
frame:RegisterEvent("PLAYER_ENTERING_WORLD")

local function CopyTable(tbl)
	if not tbl then return {} end
	local copy = {};
	for k, v in pairs(tbl) do
		if ( type(v) == "table" ) then
			copy[k] = CopyTable(v);
		else
			copy[k] = v;
		end
	end
	return copy;
end

local function PrepareForScan()
	frame:UnregisterEvent("ARTIFACT_UPDATE")
	_G.UIParent:UnregisterEvent("ARTIFACT_UPDATE")

	local ArtifactFrame = _G.ArtifactFrame
	if ArtifactFrame and not ArtifactFrame:IsShown() then
		ArtifactFrame:UnregisterEvent("ARTIFACT_UPDATE")
		ArtifactFrame:UnregisterEvent("ARTIFACT_CLOSE")
		ArtifactFrame:UnregisterEvent("ARTIFACT_MAX_RANKS_UPDATE")
	end
end

local function RestoreStateAfterScan()
	frame:RegisterEvent("ARTIFACT_UPDATE")
	_G.UIParent:RegisterEvent("ARTIFACT_UPDATE")

	local ArtifactFrame = _G.ArtifactFrame
	if ArtifactFrame and not ArtifactFrame:IsShown() then
		Clear()
		ArtifactFrame:RegisterEvent("ARTIFACT_UPDATE")
		ArtifactFrame:RegisterEvent("ARTIFACT_CLOSE")
		ArtifactFrame:RegisterEvent("ARTIFACT_MAX_RANKS_UPDATE")
	end
end

local function InformEquippedArtifactChanged(artifactID)
	if artifactID ~= equippedID then
		Debug("ARTIFACT_EQUIPPED_CHANGED", artifactID, equippedID)
		lib.callbacks:Fire("ARTIFACT_EQUIPPED_CHANGED", artifactID, equippedID)
		equippedID = artifactID
	end
end

local function InformActiveArtifactChanged(artifactID)
	local oldActiveID = activeID
	if artifactID and not GetInventoryItemEquippedUnusable("player", INVSLOT_MAINHAND) then
		activeID = artifactID
	else
		activeID = nil
	end
	if oldActiveID ~= activeID then
		Debug("ARTIFACT_ACTIVE_CHANGED", activeID, oldActiveID)
		lib.callbacks:Fire("ARTIFACT_ACTIVE_CHANGED", activeID, oldActiveID)
	end
end

local function InformTraitsChanged(artifactID)
	Debug("ARTIFACT_TRAITS_CHANGED", artifactID, artifacts[artifactID].traits)
	lib.callbacks:Fire("ARTIFACT_TRAITS_CHANGED", artifactID, CopyTable(artifacts[artifactID].traits))
end

local function StoreArtifact(itemID, altItemID, name, icon, unspentPower, numRanksPurchased, numRanksPurchasable,
	power, maxPower, tier)

	local current, isNewArtifact = artifacts[itemID], false
	if not current then
		isNewArtifact = true
		artifacts[itemID] = {
			altItemID = altItemID,
			name = name,
			icon = icon,
			unspentPower = unspentPower,
			numRanksPurchased = numRanksPurchased,
			numRanksPurchasable = numRanksPurchasable,
			power = power,
			maxPower = maxPower,
			powerForNextRank = maxPower - power,
			traits = {},
			relics = {},
			tier = tier,
		}
	else
		current.unspentPower = unspentPower
		current.numRanksPurchased = numRanksPurchased -- numRanksPurchased does not include bonus traits from relics
		current.numRanksPurchasable = numRanksPurchasable
		current.power = power
		current.maxPower = maxPower
		current.powerForNextRank = maxPower - power
		current.tier = tier
	end

	return isNewArtifact
end

local function ScanTraits(artifactID)
	local traits = {}
	local powers = GetPowers()

	for i = 1, #powers do
		local traitID = powers[i]
		local info = GetPowerInfo(traitID)
		local spellID = info.spellID
		if (info.currentRank) > 0 then
			local name, _, icon = GetSpellInfo(spellID)
			traits[#traits + 1] = {
				traitID = traitID,
				spellID = spellID,
				name = name,
				icon = icon,
				currentRank = info.currentRank,
				maxRank = info.maxRank,
				bonusRanks = info.bonusRanks,
				isGold = info.isGoldMedal,
				isStart = info.isStart,
				isFinal = info.isFinal,
				maxRanksFromTier = info.numMaxRankBonusFromTier,
				tier = info.tier,
			}
		end
	end

	if artifactID then
		artifacts[artifactID].traits = traits
	end

	return traits
end

local function ScanRelics(artifactID, doNotInform)
	local relics = artifactID and artifacts[artifactID] and artifacts[artifactID].relics or {}
	local changedSlots = {}
	local numRelicSlots = GetNumRelicSlots()

	for i = 1, numRelicSlots do
		local isLocked, name, icon, slotType, link, itemID, rank, canAddTalent = GetRelicLockedReason(i) and true or false
		if not isLocked then
			name, icon, slotType, link = GetRelicInfo(i)
			rank, canAddTalent = GetRelicSlotRankInfo(i)
			if link then
				itemID = strmatch(link, "item:(%d+):")
			end
		end

		local current = relics[i]
		if current then
			if current.itemID ~= itemID or current.isLocked ~= isLocked or
			   current.rank ~= rank or current.canAddTalent ~= canAddTalent then
				changedSlots[#changedSlots + 1] = i

				if current.itemID ~= itemID then
					current.name = name
					current.icon = icon
					current.itemID = itemID
					current.link = link
					current.talents = {}
				end
				current.isLocked = isLocked
				current.rank = rank
				current.canAddTalent = canAddTalent
			end
		else
			changedSlots[#changedSlots + 1] = i
			relics[i] = {
				type = slotType, isLocked = isLocked, name = name, icon = icon,
				itemID = itemID, link = link, rank = rank, canAddTalent = canAddTalent, talents = {},
			}
		end
	end

	if not doNotInform then
		for i = 1, #changedSlots do
			local slot = changedSlots[i]
			Debug("ARTIFACT_RELIC_CHANGED", viewedID, slot, relics[slot])
			lib.callbacks:Fire("ARTIFACT_RELIC_CHANGED", viewedID, slot, CopyTable(relics[slot]))
		end
	end

	if #changedSlots > 0 or numRelicSlots == 0 then
		ScanTraits(viewedID)
		if not doNotInform then
			InformTraitsChanged(viewedID)
		end
	end

	return relics
end

local function GetArtifactKnowledge()
	if viewedID == 133755 then return end -- exclude Underlight Angler
	local lvl = GetArtifactKnowledgeLevel()
	local mult = GetArtifactKnowledgeMultiplier()
	if artifacts.knowledgeMultiplier ~= mult or artifacts.knowledgeLevel ~= lvl then
		artifacts.knowledgeLevel = lvl
		artifacts.knowledgeMultiplier = mult
		Debug("ARTIFACT_KNOWLEDGE_CHANGED", lvl, mult)
		lib.callbacks:Fire("ARTIFACT_KNOWLEDGE_CHANGED", lvl, mult)
	end
end

local function GetViewedArtifactData()
	local itemID, altItemID, name, icon, unspentPower, numRanksPurchased, _, _, _, _, _, _, tier = GetArtifactInfo()
	if not itemID then
		Debug("|cffff0000ERROR:|r", "GetArtifactInfo() returned nil.")
		return
	end

	viewedID = itemID
	Debug("GetViewedArtifactData", name, itemID)
	local numRanksPurchasable, power, maxPower = GetNumPurchasableTraits(numRanksPurchased, unspentPower, tier)

	local isNewArtifact = StoreArtifact(
		itemID, altItemID, name, icon, unspentPower, numRanksPurchased,
		numRanksPurchasable, power, maxPower, tier
	)
	ScanRelics(viewedID, isNewArtifact)

	if isNewArtifact then
		Debug("ARTIFACT_ADDED", itemID, name)
		lib.callbacks:Fire("ARTIFACT_ADDED", itemID)
	end

	if IsViewedArtifactEquipped() then
		InformEquippedArtifactChanged(itemID)
		InformActiveArtifactChanged(itemID)
	end

	GetArtifactKnowledge()
end

local function ScanEquipped()
	if HasArtifactEquipped() then
		PrepareForScan()
		SocketInventoryItem(INVSLOT_MAINHAND)
		GetViewedArtifactData()
		Clear()
		RestoreStateAfterScan()
		frame:UnregisterEvent("UNIT_INVENTORY_CHANGED")
	end
end

local function ScanContainer(container, numObtained)
	for slot = 1, GetContainerNumSlots(container) do
		local _, _, _, quality, _, _, _, _, _, itemID = GetContainerItemInfo(container, slot)
		if quality == LE_ITEM_QUALITY_ARTIFACT then
			local classID = select(12, GetItemInfo(itemID))
			if classID == LE_ITEM_CLASS_WEAPON or classID == LE_ITEM_CLASS_ARMOR then
				Debug("ARTIFACT_FOUND", "in", container, slot)
				SocketContainerItem(container, slot)
				GetViewedArtifactData()
				Clear()
				if numObtained <= lib:GetNumObtainedArtifacts() then break end
			end
		end
	end
end

local function IterateContainers(from, to, numObtained)
	PrepareForScan()
	for container = from, to do
		ScanContainer(container, numObtained)
		if numObtained <= lib:GetNumObtainedArtifacts() then break end
	end
	RestoreStateAfterScan()
end

local function ScanBank(numObtained)
	if numObtained > lib:GetNumObtainedArtifacts() then
		PrepareForScan()
		ScanContainer(BANK_CONTAINER, numObtained)
		RestoreStateAfterScan()
	end
	if numObtained > lib:GetNumObtainedArtifacts() then
		IterateContainers(NUM_BAG_SLOTS + 1, NUM_BAG_SLOTS + NUM_BANKBAGSLOTS, numObtained)
	end
end

function private.PLAYER_ENTERING_WORLD(event)
	frame:UnregisterEvent(event)
	frame:RegisterUnitEvent("UNIT_INVENTORY_CHANGED", "player")
	frame:RegisterEvent("BAG_UPDATE_DELAYED")
	frame:RegisterEvent("BANKFRAME_OPENED")
	frame:RegisterEvent("PLAYER_EQUIPMENT_CHANGED")
	frame:RegisterEvent("ARTIFACT_CLOSE")
	frame:RegisterEvent("ARTIFACT_XP_UPDATE")
	frame:RegisterUnitEvent("PLAYER_SPECIALIZATION_CHANGED", "player")
	if _G.UnitLevel("player") < 110 then
		frame:RegisterEvent("PLAYER_LEVEL_UP")
	end
end

-- bagged artifact data becomes obtainable
function private.BAG_UPDATE_DELAYED(event)
	local numObtained = GetNumObtainedArtifacts()
	if numObtained <= 0 then return end

	-- prevent double-scanning if UNIT_INVENTORY_CHANGED fired first
	-- UNIT_INVENTORY_CHANGED does not fire after /reload
	if not equippedID and HasArtifactEquipped() then
		ScanEquipped()
	end

	if numObtained > lib:GetNumObtainedArtifacts() then
		IterateContainers(BACKPACK_CONTAINER, NUM_BAG_SLOTS, numObtained)
	end

	frame:UnregisterEvent(event)
end

-- equipped artifact data becomes obtainable
function private.UNIT_INVENTORY_CHANGED(event)
	ScanEquipped(event)
end

function private.ARTIFACT_CLOSE()
	viewedID = nil
end

function private.ARTIFACT_UPDATE(event, newItem)
	Debug(event, newItem)
	if newItem then
		GetViewedArtifactData()
	else
		if not GetNumRelicSlots() then
			Debug("|cffff0000ERROR:|r", "artifact data unobtainable.")
			return
		end
		ScanRelics(viewedID)
	end
end

function private.ARTIFACT_XP_UPDATE(event)
	-- at the forge the player can purchase traits even for unequipped artifacts
	local GetInfo = IsAtForge() and GetArtifactInfo or GetEquippedArtifactInfo
	local itemID, _, _, _, unspentPower, numRanksPurchased, _, _, _, _, _, _, tier = GetInfo()
	local numRanksPurchasable, power, maxPower = GetNumPurchasableTraits(numRanksPurchased, unspentPower, tier)

	local artifact = artifacts[itemID]
	if not artifact then
		return lib.ForceUpdate()
	end

	local diff = unspentPower - artifact.unspentPower

	if numRanksPurchased ~= artifact.numRanksPurchased then
		-- both learning traits and artifact respec trigger ARTIFACT_XP_UPDATE
		-- however respec has a positive diff and learning traits has a negative one
		ScanTraits(itemID)
		InformTraitsChanged(itemID)
	end

	if diff ~= 0 then
		artifact.unspentPower = unspentPower
		artifact.power = power
		artifact.maxPower = maxPower
		artifact.numRanksPurchased = numRanksPurchased
		artifact.numRanksPurchasable = numRanksPurchasable
		artifact.powerForNextRank = maxPower - power
		Debug(event, itemID, diff, unspentPower, power, maxPower, maxPower - power, numRanksPurchasable)
		lib.callbacks:Fire("ARTIFACT_POWER_CHANGED", itemID, diff, unspentPower, power, maxPower, maxPower - power, numRanksPurchasable)
	end
end

function private.BANKFRAME_OPENED()
	local numObtained = GetNumObtainedArtifacts()
	if numObtained > lib:GetNumObtainedArtifacts() then
		ScanBank(numObtained)
	end
end

function private.PLAYER_LEVEL_UP(event, level)
	if level < 110 then return end

	ScanEquipped()
	frame:UnregisterEvent(event)
end

function private.PLAYER_EQUIPMENT_CHANGED(event, slot)
	if slot == INVSLOT_MAINHAND then
		local itemID = GetEquippedArtifactInfo()

		if itemID and not artifacts[itemID] then
			ScanEquipped(event)
		end

		InformEquippedArtifactChanged(itemID)
		InformActiveArtifactChanged(itemID)
	end
end

-- needed in case the game fails to switch artifacts
function private.PLAYER_SPECIALIZATION_CHANGED(event)
	local itemID = GetEquippedArtifactInfo()
	Debug(event, itemID)
	InformActiveArtifactChanged(itemID)
end

function lib.GetActiveArtifactID()
	return activeID
end

function lib.GetArtifactInfo(_, artifactID)
	artifactID = tonumber(artifactID) or equippedID
	return artifactID, CopyTable(artifacts[artifactID])
end

function lib.GetAllArtifactsInfo()
	return CopyTable(artifacts)
end

function lib.GetNumObtainedArtifacts()
	local numArtifacts = 0
	for artifact in pairs(artifacts) do
		if tonumber(artifact) then
			numArtifacts = numArtifacts + 1
		end
	end

	return numArtifacts
end

function lib.GetArtifactTraitInfo(_, id, artifactID)
	artifactID = artifactID or equippedID
	local artifact = artifacts[artifactID]
	if id and artifact then
		local traits = artifact.traits
		for i = 1, #traits do
			local info = traits[i]
			if id == info.traitID or id == info.spellID then
				return CopyTable(info)
			end
		end
	end
end

function lib.GetArtifactTraits(_, artifactID)
	artifactID = tonumber(artifactID) or equippedID
	local artifact = artifacts[artifactID]
	if not artifact then return end
	return artifactID, CopyTable(artifact.traits)
end

function lib.GetArtifactRelics(_, artifactID)
	artifactID = tonumber(artifactID) or equippedID
	local artifact = artifacts[artifactID]
	if not artifact then return end
	return artifactID, CopyTable(artifact.relics)
end

function lib.GetArtifactPower(_, artifactID)
	artifactID = tonumber(artifactID) or equippedID
	local artifact = artifacts[artifactID]
	if not artifact then return end
	return artifactID, artifact.unspentPower, artifact.power, artifact.maxPower,
	       artifact.powerForNextRank, artifact.numRanksPurchased, artifact.numRanksPurchasable
end

function lib.GetArtifactKnowledge()
	return artifacts.knowledgeLevel, artifacts.knowledgeMultiplier
end

function lib.GetAcquiredArtifactPower(_, artifactID)
	local total = 0

	if artifactID then
		local data = artifacts[artifactID]
		total = total + data.unspentPower
		local rank = 1
		while rank < data.numRanksPurchased do
			total = total + GetCostForPointAtRank(rank, data.tier)
			rank = rank + 1
		end

		return total
	end

	for itemID, data in pairs(artifacts) do
		if tonumber(itemID) then
			total = total + data.unspentPower
			local rank = 1
			while rank < data.numRanksPurchased do
				total = total + GetCostForPointAtRank(rank, data.tier)
				rank = rank + 1
			end
		end
	end

	return total
end

function lib.GetArtifactPowerFromItem(_, item)
	local itemID, knowledgeLevel = tonumber(item), 1
	if not itemID then
		itemID, knowledgeLevel = item:match("item:(%d+).-(%d*):::|h")
		if not itemID then return end
		knowledgeLevel = tonumber(knowledgeLevel) or 1
	end

	if IsArtifactPowerItem(itemID) then
		local _, _, spellID = GetItemSpell(itemID)
		return artifactPowerData.multiplier[knowledgeLevel] * (artifactPowerData.spells[spellID] or 0)
	elseif artifactPowerData.items[itemID] then
		return artifactPowerData.multiplier[knowledgeLevel] * artifactPowerData.items[itemID]
	end
end

function lib.ForceUpdate()
	if _G.ArtifactFrame and _G.ArtifactFrame:IsShown() then
		Debug("ForceUpdate", "aborted because ArtifactFrame is open.")
		return
	end
	local numObtained = GetNumObtainedArtifacts()
	if numObtained > 0 then
		ScanEquipped("FORCE_UPDATE")
		IterateContainers(BACKPACK_CONTAINER, NUM_BAG_SLOTS, numObtained)
	end
end

local function TraitsIterator(traits, index)
	index = index and index + 1 or 1
	local trait = traits[index]
	if trait then
		return index, trait.traitID, trait.spellID, trait.name, trait.icon, trait.currentRank, trait.maxRank,
		       trait.bonusRanks, trait.isGold, trait.isStart, trait.isFinal
	end
end

function lib.IterateTraits(_, artifactID)
	artifactID = tonumber(artifactID) or equippedID
	local artifact = artifacts[artifactID]
	if not artifact then return function() return end end

	return TraitsIterator, artifact.traits
end
